---
description: Welcome to the advanced Libre user section! Congrats on finishing level 1.
---

# Libre Level 2 - Advanced

1. <mark style="background-color:orange;">How to optimize staking rewards.</mark>

Staking your LIBRE earns rewards - the rewards are higher the earlier you stake, and the longer you stake. You can get LIBRE using https://defi.libre.org/swap or in the Bitcoin Libre mobile app. Then you can stake by going to https://defi.libre.org/wallet and clicking the "New Stake" button. Compare the rewards for staking to the rewards for farming liquidity rewards which require no time lockup.

Sections to visit next: [staking.md](../earn/staking.md "mention") and [staking-details.md](../technical-details/staking-details.md "mention")

2. <mark style="background-color:orange;">How to build or integrate Libre.</mark>

Libre is easy to integrate with a few short lines of code for authentication and payments. See the [libre-developer-tools](../the-libre-blockchain/building-on-libre/libre-developer-tools/ "mention") section of this site.

3. <mark style="background-color:orange;">How to set up a Libre validator node.</mark>

Libre validators run the network, start here to learn more [become-a-validator.md](../earn/become-a-validator.md "mention")

4. <mark style="background-color:orange;">How to earn LIBRE by providing liquidity (farm).</mark>

Another way to earn LIBRE is by providing liquidity to Libre swap liquidity pools which are permissionless and decentralized [https://defi.libre.org](https://defi.libre.org/). Go to [https://defi.libre.org/swap](https://defi.libre.org/swap) and click on Pool. See also[farming.md](../earn/farming.md "mention")

5. <mark style="background-color:orange;">How to create multiple accounts on Libre.</mark>

You can use the [anchor-wallet](../accounts-and-wallets/anchor-wallet/ "mention") on Mac, Windows, or Linux to generate your own keys and create accounts on Libre. See the following section of this site for more details:[creating-multiple-accounts.md](../accounts-and-wallets/creating-multiple-accounts.md "mention")
