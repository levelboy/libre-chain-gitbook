---
description: Welcome to Libre! Ready to learn about Libre - we've got you covered.
---

# Libre Level 1 - Beginner

Welcome to the Libre blockchain.

Most people are shocked at how easy Libre is to use. Most blockchains are built for developers and this is a blockchain built for everyone.&#x20;

1. <mark style="background-color:orange;">What is Libre?</mark>

Libre is Freedom. Libre is a New Blockchain that BUILDS BRIDGES to make things fast, secure, scalable, and easy-to-use. No Tolls or Fees. No gatekeepers.&#x20;

Libre is completely Decentralized, Fair Launched, and the Eco-Friendliest Blockchain with the lowest energy usage per daily transaction max.

Libre was conceived and launched in El Salvador in 2021.

2. <mark style="background-color:orange;">Why is Libre unique?</mark>

Libre's solves the "Trilemma" of blockchains - it is a "Trifecta" - **scalable**, **decentralized**, and **secure**. It is also very easy to use due to having simple usernames instead of addresses and no transaction fees.

3. <mark style="background-color:orange;">Who is behind Libre?</mark>

Libre was designed and built by several groups working together.

* Edenia - smart contract development team out (Costa Rica)
* Bizan Bizz, SA - backend and frontend development team (Panama)
* Various other contributors around the world.

4. <mark style="background-color:orange;">Read Libre Whitepaper.</mark>

It's right here on this site, there is information about [Technical Details](broken-reference), [Governance](broken-reference), [Interoperability](broken-reference), the [LIBRE coin](broken-reference).

5. <mark style="background-color:orange;">How to get started.</mark>

Download the [Bitcoin Libre wallet](https://bitcoinlibre.io). Make sure to backup your seed phrase so you don't lose any funds.

6. <mark style="background-color:orange;">How to get the LIBRE coin.</mark>

LIBRE trades on the decentralized Libre Swap - you can send in some Bitcoin into Bitcoin Libre and go to [https://defi.libre.org/swap](https://defi.libre.org) to trade for LIBRE.&#x20;

7. <mark style="background-color:orange;">How to stake LIBRE.</mark>

LIBRE staking is where you lock up your LIBRE for a time period chosen by you. This is your commitment to being a part of LIBRE for a specified time duration. The longer you stake, the higher your rewards. The earlier you begin, the higher your rewards. You can do this at [https://defi.libre.org/wallet](https://defi.libre.org/wallet)&#x20;

8. <mark style="background-color:orange;">Join the Libre community.</mark>

Join the many Libre communities - start with the main [Discord](https://discord.com/invite/asJNsB6sYJ) and [Telegram](https://t.me/LibreBlockchain) community groups. Start one for your language if it doesn't exist yet.

For the latest info - follow [Libre on Twitter.](https://twitter.com/libreblockchain)

9. <mark style="background-color:orange;">Libre Philosophy</mark>

‘The technical aspects of the automobile seemed daunting at first compared with the horse, yet its superior reliability and speed have made the technical knowledge needed to operate a fair price to pay for most people.’&#x20;

Libre has been built with the aim of seemingly riding the horse, with the function and speed built-in for maximum usability and easy adoption. &#x20;

\
