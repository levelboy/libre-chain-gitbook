---
description: A complete explanation about our the LIBRE spindrop.
---

# Eligibility Requirements for the LIBRE Spindrop

The LIBRE airdrop is available for the following chains and wallets:\
\
\- Solana / Phantom\
\- Ethereum / MetaMask\
\- EOS / Anchor\
\- Telos / Anchor\
\- WAX / Anchor\
\- Proton / Anchor\
\- Proton (Libre wallet generated) / Bitcoin Libre or Anchor\
\
A minimum of a specific amount (around $1) of each currency at the snapshot date is required to claim the snapshot. This amount has been set, and the snapshot is final. No exceptions wil be made.
