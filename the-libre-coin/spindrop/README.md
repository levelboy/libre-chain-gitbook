---
description: >-
  The Libre Spindrop is an airdop of tokens randomly decided by spinning a
  wheel. Good luck!
---

# Spindrop

10,000,000 LIBRE has been allocated to be claimed via a broad-based airdrop. As of Oct 27, 2022 - only 110,000 LIBRE have been claimed.

The airdrop is available to qualified accounts on Solana, Ethereum, Proton, EOS, Telos, and WAX.&#x20;

Near the end of the Mint Rush, on Dec 13, any unclaimed coins will sent to the Libre DAO.

Until then, you can view the unclaimed coins here: [https://lb.libre.org/v2/explore/account/drop.libre](https://lb.libre.org/v2/explore/account/drop.libre)&#x20;

[eligibility-requirements-for-the-libre-spindrop.md](eligibility-requirements-for-the-libre-spindrop.md "mention")

[how-to-claim-the-libre-spindrop.md](how-to-claim-the-libre-spindrop.md "mention")
