---
description: Information and estimates of the LIBRE coin supply.
---

# Supply

<figure><img src="../.gitbook/assets/supply (1).jpg" alt=""><figcaption><p>Prediction of token distribution created in October 2022.</p></figcaption></figure>

<figure><img src="../.gitbook/assets/image.png" alt=""><figcaption><p>Screen capture of Jan 15, 2023 LIBRE supply and market cap.</p></figcaption></figure>

### History and Distribution

Libre mainnet was launched on July 4, 2022. Initially, there were 10 validators which have grown to 21+ by Oct 2022 and 27+ by Jan 2023. Validators each earn 4 LIBRE per block that they produce. A block is mined every 0.5 seconds. Validators rotate through production every 12 seconds. There is a halvening every 6 months for the block rewards (total of 2 halvenings). Approximately 113M LIBRE will be owned by validators by Dec 15, 2022 and 90% of it will be staked for an average of 264 days. The supply of LIBRE is always [100% transparent and visible on-chain](https://lb.libre.org/v1/chain/get\_currency\_stats?code=eosio.token\&symbol=LIBRE).

{% hint style="info" %}
**No Premine**

Libre has "no premine" - meaning that no tokens were given for free to any founders or team - the chain was launched with an initial issuance of only 4200 LIBRE and it was staked to vote in the initial validators who had been running the Phoenix Testnet for the previous 6-8 months.&#x20;

These are the accounts, which can be validated on-chain in the Libre block explorer:&#x20;

`genvote1, genvote2, genvote3, genvote4, genvote5, genvotea, genvoteb, genvotec, genvoted, genvotee, genvotef, genvote12, genvote13, genvote14, genvote15, genvotei, genvotej`

These stakes were claimed (a total of 4209.1045 LIBRE) and donated to Libre DAO (dao.libre) after the initial voting period.
{% endhint %}

10M LIBRE was allocated to an airdrop that began July 13, 2022 - as of Jan, 2023 only \~277,000 LIBRE have been claimed. The airdrop will conclude on Jan 25, 2022 and the remaining tokens will be allocated to the DAO treasury controlled by all token holders.

On December 15, 2022 the Mint Rush began accepting contributions. There was 200M LIBRE available to mint. At the end of the Mint Rush, a matching amount of 200M LIBRE was placed into the AMM liquidity pool (swap.libre) together with all of the BTC contributed to the Mint Rush.&#x20;

The Mint Rush staking yields are as much as 3x higher than existing staking yields for validators.

Participants on Day 0 of Mint Rush can earn up to 400% APY vs current yields of 130% (approx staking return for LIBRE staking on Dec 15, 2022). This will result in much higher tokens at the end of the staking periods for the Mint Rush participants as a category. If these are re-staked, the Mint Rush participants will be in majority voting control of the chain.

As of Jan 2023, there is no account with more than 9% of the supply based on the current stakes ending in the next 4 years.

The active supply and market cap can be viewed on the [Defi Dashboard](https://defi.libre.org).
