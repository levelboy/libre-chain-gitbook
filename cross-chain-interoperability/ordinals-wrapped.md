---
description: Libre has the first ecosystem built around wrapped ordinals.
---

# Ordinals (wrapped)

Every user of Libre gets a taproot address for using wrapped ordinals. The taproot address provided uses an xpub that is a multisig of Libre validators.&#x20;

These Libre validators also run Bitcoin nodes and we will soon use PSBTs transmitted on-chain for unwrapping of ordinals. This aspect is currently under development. If you are interested in assisting, please reach out to the team at [Agile Freaks](https://www.agilefreaks.com/).
