---
description: Bitcoin Lightning compatibility with Libre Chain
---

# Bitcoin Lightning Network

You don't have to do anything special to send and receive Bitcoin to or from Libre using Lightning when you use the Bitcoin Libre wallet.&#x20;

Libre has been tested to work with every major Lightning-compatible wallet:&#x20;

* Muun
* Breez
* Wallet of Satoshi
* Strike
* Chivo
* Phoenix
* Blue Wallet.

Libre seamlessly connects with Bitcoin Lightning Network via [Libre's Lightning Providers](../earn/libre-lightning-provider.md) (LLPs) which provide liquidity and swaps for [PBTC](bitcoin-mainnet.md) to BTC on Lightning.
