# pNetwork

The pNetwork is a secure network of open-source distributed nodes that providce transparent and provable pegging of tokens with interoperability between chains like Bitcoin and Ethereum.

pNetwork built on Libre as well on Polygon, BSC, Telos, Algorand, EOS, XDAI, and Ultra.

If you have an issue with pNetwork, you can get support [here on the pNetwork Telegram](https://t.me/pNetworkDefi).

## pNetwork Bridge dApp

[**pNetwork Bridge**](https://dapp.ptokens.io) **- this is where you can move your tokens between Libre and other chains. Here are some screenshots:**

<figure><img src="../.gitbook/assets/image (6).png" alt=""><figcaption><p>PNetwork Bridges Bitcoin and 9 other chains including Libre</p></figcaption></figure>

<figure><img src="../.gitbook/assets/image (3).png" alt=""><figcaption><p>PNetwork Bridges Tether and 4 other blockchains including Libre</p></figcaption></figure>

## pNetwork Links

**Here are some additional links to the pNetwork where you can dig deeper:**

[**pNetwork Code Audit**](https://www.certik.com/projects/pnt#trustScore) **(**[**Certik** ](https://www.certik.com/projects/pnt#trustScore)**)**

[**pTokens TVL**](https://p.network/ptokens)

[**pNetwork Wiki**](https://docs.p.network/)

[**pNetwork Stats (Grafana Dashboard)**](https://pnetwork.watch/d/cQs1gKuGkaa/pnetwork-stats?orgId=1)

[**pNetwork Github (Provable Things)**](https://github.com/provable-things)

