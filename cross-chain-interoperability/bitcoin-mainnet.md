---
description: Bitcoin on Libre via pNetwork.
---

# Bitcoin Mainnet

Libre connects to Bitcoin via Lightning and the pNetwork.

PNetwork has integrated Libre as one of the 16 chains they currently support for their PBTC.&#x20;

PBTC is [transparent](https://dashboard.libre.org/audit), decentralized, and censorship-free.&#x20;

PBTC utilizes [PNetwork](pnetwork.md) oracle technology which has cross-chain liquidity to many other chains and is not dependent on Libre validators or governance.



The PNetwork Bitcoin bridge supports **BTC > PBTC (Libre)** and **PBTC (Libre) > BTC**.

[https://dapp.ptokens.io/swap?asset=btc\&from=btc\&to=libre](https://dapp.ptokens.io/swap?asset=btc\&from=btc\&to=libre)\
\
You can also get your BTC deposit address from the API\
\
[https://core-api.libre.org/ptokens/ptoken-wrapping/\<account>”](https://core-api.libre.org/ptokens/ptoken-wrapping/%3Caccount%3E%E2%80%9D)\
\
example: https://core-api.libre.org/ptokens/ptoken-wrapping/tokyo

