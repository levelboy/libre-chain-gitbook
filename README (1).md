---
description: Fast Bitcoin and Tether + Ordinals
---

# Founding Principles

### Founding Principles

Libre is about Freedom

Libre Builds Bridges

Libre is the fastest and easiest way to transfer wealth worldwide, at no cost

Libre was built with the end user (you) in mind

Libre follows the principles of self sovereignty, transparency, and decentralization

Libre knows that the faster and easier transactions are made with safety and security, the faster we can build a better world.

Libre was founded on fairness and transparency, mirroring how we think communities should be run

Libre is focused on easy and thorough adoption, inclusive to all&#x20;

Libre believes education will foster wide scale acceptability and adoption of this superior decentralized system&#x20;

Libre supports and amplifies the Bitcoin Blockchain by making it scalable, free and fast

Elements of Libre\
Usability
---------

* Easy to Use Mobile Wallet
* BRC20 Marketplace
* DeFi
* Libre Coin
* Community

### A new sidechain focused on powering adoption.

Bitcoin, Tether, and Ordinals on Libre are fast, programmable, and actually easy to use.

Libre features simple usernames, simple user interfaces and simple inter-chain connectivity to make Bitcoin and Tether accessible to anyone, anywhere.

Libre is a delegated-proof-of-stake blockchain, with non-custodial wrapping (pegged) assets provided by [pNetwork](cross-chain-interoperability/pnetwork.md), and simple on-and-off ramps to Bitcoin using the Lightning Network. It's completely decentralized, with no central point of failure. There are daily audits you can find on the [DeFi dashboard](https://defi.libre.org).

Libre also features the **first wrapped ordinals** - there is a new [BRC20 marketplace](https://ordinals.libre.org) which just launched in "experimental mode." Ordinals can be sent in and out of Libre to any Bitcoin wallet without permission.

Libre is a collective effort. It combines technologies built by a collective of developers and Bitcoin-enthusiasts from the USA, Italy, UK, Costa Rica, Argentina, Georgia, Lithuania, and Serbia. It also has [nodes all around the world](https://libre.antelope.tools/nodes-distribution).&#x20;

## Simple Interfaces

Libre is the easiest blockchain to use and build on - accounts are free and you do not need to acquire tokens to use Libre. The speed, reliability, and cost of transactions is equal to the utility that people around the world have previously only been able to enjoy when using centralized applications - now Libre chain provides that level of functionality.

## Fair Launch

There are no LIBRE coins allocated to team, investors, or funds. The LIBRE token distribution starts with an [airdrop](the-libre-coin/spindrop/) and block mining rewards that are paid to the elected validators for each block processed. LIBRE holders can engage in [governance](broken-reference) through the Decentralized Autonomous Organization ([DAO](https://defi.libre.org/dao)) and through the [validator elections](governance/validator-election.md).

## Freedom

With Libre, users have the freedom to transact, trade, build, [earn](broken-reference) and self-govern. The etymology of the name Libre comes from the latin _liber_ meaning "free, unrestricted."&#x20;
