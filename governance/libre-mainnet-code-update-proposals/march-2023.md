---
description: Multisig and code updates for March 2023.
---

# March 2023

## Multisig 1/3 - "[daoperm](https://msig.app/libre/quantum/daoperm)"

#### Link&#x20;

[https://msig.app/libre/quantum/daopermfix](https://msig.app/libre/quantum/daoperm)

#### Summary

Updates active and owner permissions of dao.libre to be controlled by 2/3+1 validators to update code, countvotes, and approve or cancel (veto) proposals.&#x20;

### Requested signers

all active producers

### Code Changes

none - but here is how to verify the actions packed in here:

1. Grab the hex data from the multisig action: `0000000000ea3055026f3c6400000000000000000000020000000000ea30550040cbdaa86c52d501000050f7b808a84900000000a8ed323231000050f7b808a8490000000080ab26a700000000000000000100000000010070a2b702ea305500000000a8ed32320100000000000000ea30550040cbdaa86c52d501000050f7b808a84900000000a8ed323243000050f7b808a84900000000a8ed32320000000080ab26a7010000000002000050f7b808a84900804a1401ea305501000070a2b702ea305500000000a8ed323201000000`
2. Run convert unpack\_action\_data:

`libre.sh convert unpack_action_data eosio.wrap exec 0000000000ea3055026f3c6400000000000000000000020000000000ea30550040cbdaa86c52d501000050f7b808a84900000000a8ed323231000050f7b808a8490000000080ab26a700000000000000000100000000010070a2b702ea305500000000a8ed32320100000000000000ea30550040cbdaa86c52d501000050f7b808a84900000000a8ed323243000050f7b808a84900000000a8ed32320000000080ab26a7010000000002000050f7b808a84900804a1401ea305501000070a2b702ea305500000000a8ed323201000000`

3. Grab the hexes from that convert and unpack those:

`libre.sh convert unpack_action_data eosio updateauth 000050f7b808a8490000000080ab26a700000000000000000100000000010070a2b702ea305500000000a8ed3232010000`

`libre.sh convert unpack_action_data eosio updateauth 000050f7b808a84900000000a8ed32320000000080ab26a7010000000002000050f7b808a84900804a1401ea305501000070a2b702ea305500000000a8ed3232010000`

You should get:

`{ "account": "dao.libre", "permission": "active", "parent": "owner", "auth": { "threshold": 1, "keys": [], "accounts": [{ "permission": { "actor": "dao.libre", "permission": "eosio.code" }, "weight": 1 },{ "permission": { "actor": "eosio.prods", "permission": "active" }, "weight": 1 } ], "waits": [] } }`

`{ "account": "dao.libre", "permission": "owner", "parent": "", "auth": { "threshold": 1, "keys": [], "accounts": [{ "permission": { "actor": "eosio.prods", "permission": "active" }, "weight": 1 } ], "waits": [] } }`

## Multisig 2/3 - “[returnfunds1](https://msig.app/libre/blocktime/returnfunds1)”

**Executed tx:** [e0dee081bbf1cff136f73576f83253b6ac8f02841d6f91587085dbe53671ce31](https://www.libreblocks.io/tx/e0dee081bbf1cff136f73576f83253b6ac8f02841d6f91587085dbe53671ce31)

#### Summary

Return funds accidentally sent to system account.&#x20;

### Requested signers

all active producers

### Code Changes

none

## Multisig 3/3 - “approvedao”

#### Executed tx: [4178a65fed0de802ee6c1434d3219413922261298a0455f40f150872653a9551](https://www.libreblocks.io/tx/4178a65fed0de802ee6c1434d3219413922261298a0455f40f150872653a9551)

#### Summary

Part 1 - Approves payment from DAO to Edenia for Jan development as proposed and voted on here: [https://dashboard.libre.org/dao/edeniajandev](https://dashboard.libre.org/dao/edeniajandev)&#x20;

Outcome of this part 1 is that 1787395.0000 LIBRE will be paid to `edeniaedenia.`

Part 2 - Approves the proposal for additional DAO funding here: [https://dashboard.libre.org/dao/3yiz3avosj2n](https://dashboard.libre.org/dao/3yiz3avosj2n)

Outcome of this part 2  is that 10000.0000 LIBRE will be paid to `libretech`

Simple refund of DAO Proposal costs. Next, validators will need to issue the 100M LIBRE to the dao.libre account with a multisig.

### Requested signers

all active producers

### Code Changes

**N/A**



###

| <p><br></p> | Currently Deployed | Proposed Update |
| ----------- | ------------------ | --------------- |
| Commit hash |                    |                 |
| sha256sum   |                    |                 |

**Code comparison (proposed to deployed):**

TBD
