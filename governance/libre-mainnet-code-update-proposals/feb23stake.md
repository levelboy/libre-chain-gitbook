---
description: 3 proposals planned for February 2023
---

# February 2023

## Multisig 1/3 - "msigupdate"

#### Summary

Updates eosio.msig to the newest version from antelope's main repo.

Contracts version 1.9.2 had a version of eosio.msig that used deferred tx which has been deprecated - this will execute actions individually and it is currently the version running on other Antelope networks.

**Executed TX:** [d33463c3fb98029d28d6b5750e3c6886ea1f3858478a652b938e9a142781097b](https://www.libreblocks.io/tx/d33463c3fb98029d28d6b5750e3c6886ea1f3858478a652b938e9a142781097b)

### Requested signers

all active producers

### Code Changes

**Repo:** [https://gitlab.com/libre-chain/libre-chain-contracts](https://gitlab.com/libre-chain/libre-chain-contracts)

**Path to WASM in repo:**

* contracts/eosio.msig/eosio.msig.wasm

| <p><br></p> | Currently Deployed                                               | Proposed Update                                                  |
| ----------- | ---------------------------------------------------------------- | ---------------------------------------------------------------- |
| Commit hash | 4c412311cc188438de2c5b4a7701921ec41c7ee8                         | 86acd956f15659cab10fb99df62c4a8420b8c805                         |
| sha256sum   | 1040fbe8e491ab6038b64b4675310ebcf9218dc1e69ec2712ef0b11db687c8ff | faa87ac1cbf719544f32de5e652e72b0080bca2ddede44ad1a104e3c5768b225 |

**Files updated in this WASM:**

* contracts/eosio.msig/include/eosio.msig/eosio.msig.hpp
* contracts/eosio.msig/src/eosio.msig.cpp

**Code comparison (proposed to deployed):**

Note: This is an old commit for the original file so there are lots of code changes in this comparison which do not apply to this single update. Only the files mentioned above need to be reviewed.

[https://gitlab.com/libre-chain/libre-chain-contracts/-/compare/4c412311cc188438de2c5b4a7701921ec41c7ee8...86acd956f15659cab10fb99df62c4a8420b8c805?from\_project\_id=37449894\&page=2\&straight=false](https://gitlab.com/libre-chain/libre-chain-contracts/-/compare/4c412311cc188438de2c5b4a7701921ec41c7ee8...86acd956f15659cab10fb99df62c4a8420b8c805?from\_project\_id=37449894\&page=2\&straight=false)

## Multisig 2/3 - “feb23stake”

#### Summary

Updates stake.libre to calculate voting power - implements changes recommended by Certik from the audit regarding this function as well as some other basic changes.

These changes have been tested on local testnet as well as the Libre testnet. You can verify that the sha256sum is the same version which is deployed on testnet currently.

This Multisig updates stake.libre contract to add migrate1, migrate2, and updatevp actions as well as add the "power" table.

**Executed Tx:** [43abf3979d5dacfac36d0ed55f3b9a778b6551f38aba18069b68c6ef5385808a](https://www.libreblocks.io/tx/43abf3979d5dacfac36d0ed55f3b9a778b6551f38aba18069b68c6ef5385808a)

### Requested signers

quantum, cryptobloks, hotstart, rioblocks, eosphere

### Code Changes

**Repo:** [https://gitlab.com/libre-chain/staking-contract/](https://gitlab.com/libre-chain/staking-contract/)

**Path to WASM in repo:**

* stakingtoken/stakingtoken.wasm

| <p><br></p> | Currently Deployed                                               | Proposed Update                                                  |
| ----------- | ---------------------------------------------------------------- | ---------------------------------------------------------------- |
| Commit hash | 539476403d1c6f86d1e1176f41aceef21a0cd118                         | 9c3a3bc7393afc052e94122ffbaa7bca1dcfe37b                         |
| sha256sum   | e285a511028be0dd73c5bd6090ebe775b7d2565c8682e90b30afa5636636c896 | 6962b0ee319957fb1783fcdcf8fa5ed029867bf1e7208522694664bd6607fa98 |

**Files updated in this WASM:**

* stakingtoken/include/constants.hpp
* stakingtoken/include/stakingtoken.hpp
* stakingtoken/src/stakingtoken.cpp

**Code comparison (update to deployed):**

[https://gitlab.com/libre-chain/staking-contract/-/compare/539476403d1c6f86d1e1176f41aceef21a0cd118...9c3a3bc7393afc052e94122ffbaa7bca1dcfe37b?from\_project\_id=37449753\&straight=false\&view=parallel](https://gitlab.com/libre-chain/staking-contract/-/compare/539476403d1c6f86d1e1176f41aceef21a0cd118...9c3a3bc7393afc052e94122ffbaa7bca1dcfe37b?from\_project\_id=37449753\&straight=false\&view=parallel)



## Multisig 3/3 - “systemupdate”

#### Summary

Updates eosio to reference voting power in stake.libre::power for choosing validators

[needs to be proposed](https://local.bloks.io) after Feb multisig2/3 - currently being prod tested on Testnet still and delayed until April.

### Requested signers

all active producers

### Code Changes

**Repo:** [https://gitlab.com/libre-chain/libre-chain-contracts](https://gitlab.com/libre-chain/libre-chain-contracts)

**Path to WASM in repo: TBD**

| <p><br></p> | Currently Deployed | Proposed Update |
| ----------- | ------------------ | --------------- |
| Commit hash |                    |                 |
| sha256sum   |                    |                 |

**Code comparison (proposed to deployed):**

TBD
