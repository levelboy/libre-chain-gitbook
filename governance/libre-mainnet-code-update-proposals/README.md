---
description: Code updates require a multisig of the 2/3+1 of active validators.
---

# Libre Mainnet Code Update Proposals

## Code Update Process

All smart contract updates for **core** functionality follow this process:

1. Unit tests written and passed by developer
2. Testing is complete on a local dev net (staging)
3. Testing is complete on Libre testnet (mimics mainnet)
4. 2/3+1 Validators have reviewed the code and approved it following the steps in [verifying-code-updates.md](../../technical-details/verifying-code-updates.md "mention").

Active validators must verify and approve code changes to the Libre mainnet.&#x20;

Versions of code deployed on mainnet/testnet can be viewed in [deployed-smart-contract-versions.md](../../technical-details/deployed-smart-contract-versions.md "mention").

## List of Proposals

This is a list of monthly Libre mainnet code change proposals that can be verified on-chain.

* [march-2023.md](march-2023.md "mention") (PENDING)
* [feb23stake.md](feb23stake.md "mention") (PARTIALLY APPROVED)
* [dec22updates.md](dec22updates.md "mention") (APPROVED)
