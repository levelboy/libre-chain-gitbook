---
description: Libre DAO - propose and vote
---

# Libre DAO

The DAO is the Decentralized Autonomous Organization which has a treasury and can allocate funds from the treasury for proposals made by token holders to improve the chain or build / integrate applications. It works on the process of 1 token (LIBRE) \* staked period = voting power.

By design, the DAO does not have power to change the core constants and smart contracts of Libre. That power lies in the validator multisig which requires 2/3+1 vote of all active validators. You can view these code changes here under [libre-mainnet-code-update-proposals](libre-mainnet-code-update-proposals/ "mention").

## Libre DAO Operations

The general operation of the DAO is that proposals are made, voted on for a multiple day period and then either passed or rejected.&#x20;

The DAO requires at least 10% of all [Voting Power](voting-power.md) to be voted on a proposal -  51% of the voting power for / against will decide.

Once a proposal is passed, the validators must sign a 2/3+1 multisig to distribute funds from the DAO treasury.

The DAO is perpetually funded from the [dao-tax.md](../earn/dao-tax.md "mention").

## DAO Proposals

Anyone can make a DAO proposal for the cost set in the dao.libre contract tables.&#x20;

Initially, this value is 10,000 LIBRE, but it can be changed by updating the dao.libre:params2 table.&#x20;

DAO Proposals have the following status as defined [here](https://gitlab.com/libre-chain/btc-libre-governance/-/blob/master/contracts/btclgovernan/include/btclgovernan.hpp) in the smart contract:

```
        DRAFT = 1,
        ACTIVE = 2,
        SUCCEEDED = 3,
        DEFEATED = 4,
        CANCELED = 5,
        COMPLETED = 6,
```

Draft - this is the initial state of a proposal, once the fee has been paid, it will become "active."

Active - a proposal that is active can be voted on using voting power, the proposal will remain active for the voting period as defined in the dao.libre:params2 table (initially 10 days). The command `checkvotes` can be run by any Libre account to update the votes every 5 min.

Succeeded - once a proposal voting period has ended, the command `countvotes` can be run by a 2/3+1 multisig of active validators to get a final count of the votes FOR and AGAINST the proposal. Next it can be approved or cancelled by a 2/3+1 multisig of the active validators.

Defeated - if a proposal fails to get 10% of the total voting power and also a majority of the votes FOR, then it will be defeated.&#x20;

Cancelled - once a proposal succeeds, a 2/3+1 multisig of validators can act as a veto and the proposal will have the status of cancelled. This is the final stage for the proposal. If cancelled, no funds are distrubuted.

Completed - once a proposal succeeds, a 2/3+1 multisig of validators using the action dao.libre::approve will execute the proposal and transition the status in the smart contract to "Completed." This is the final stage of a proposal and funds will be distributed according to the proposal.
