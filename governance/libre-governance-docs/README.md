---
description: https://gitlab.com/libre-chain/libre-governance
---

# Libre Governance Docs

### Governace Documents Repository for Libre Blockchain

This [repository](https://gitlab.com/libre-chain/libre-governance) holds all foundational documents for the Libre Blockchain. These are intended to provide a transparent set of common rules that will guide all users in interactions with the network.

{% hint style="info" %}
The version shown here is accurate to this commit hash on the repository: [9d516d7efa16f40c38fea83b828f959bdd52cb35](https://gitlab.com/libre-chain/libre-governance/-/tree/9d516d7efa16f40c38fea83b828f959bdd52cb35)
{% endhint %}

### The Documents <a href="#user-content-the-documents" id="user-content-the-documents"></a>

Libre has three foundational governance documents, the Libre Blockchain Operating Agreement (LBOA), which applies to all users and the Block Producers Minimum Requirements and Block Producers Regproducer agreements which only apply to those accounts that register as block producer candidates.

### Status of Documents <a href="#user-content-status-of-documents" id="user-content-status-of-documents"></a>

These documents are currently proposals only, modified from the Telos governance documents to suit the specific needs of the Libre Blockchain. Currently, neither these documents, nor any others have any validity in defining the accepted rules and regulations for operations of the Libre Blockchain. The Libre Governance Workgroup has been asked by the current group of Libre block producers to prepare these initial documents for discussion and eventual ratification.

### Amending These Documents <a href="#user-content-amending-these-documents" id="user-content-amending-these-documents"></a>

As there are as yet, no ratified governance documents for the Libre Blockchain, the first process required of the Libre block producers will be to modify these documents or others proposed for that purpose to be accepted as the Libre governance documents and ratify them by an on-chain multi-signature vote of the block producers. Once this occurs the method and requirements for further amending the documents will be described within the ratified governace documents. Proposals may be recorded in this repository but will only become effective by the ratification of a majority of the elected Libre block producers.

### &#x20;<a href="#user-content-contributing" id="user-content-contributing"></a>
