---
description: >-
  https://gitlab.com/libre-chain/libre-governance/-/blob/main/OperatingAgreement.md
---

# Libre Blockchain Operating Agreement (LBOA)

## Libre Blockchain Operating Agreement (LBOA)

This Agreement is a multi-party contract entered into by the Members by virtue of their use of this blockchain.

### 1. Preamble <a href="#user-content-1-preamble" id="user-content-1-preamble"></a>

This document is intended to set common agreements among all users of the Libre Blockchain in order to establish a secure, stable, and governable blockchain where value and information can be stored and all disputes shall be arbitrated solely using the methods described herein and within the "Libre Governance Documents" comprised of this Agreement, the "Libre Block Producer Minimum Requirements" and the Libre "regproducer" contract human language terms. Each document in the Libre Governance Documents is hereby incorporated herein by reference as if fully set forth herein. The Libre Blockchain is composed of Members (as defined below) who have chosen to organize themselves using a computer blockchain as a form of transnational exchange of value, information, and commerce. These Members have agreed to govern the Libre Blockchain, its transactions and native coin by means of this Libre Blockchain Operating Agreement (this "Agreement") which enumerates a set of mutual representations and agreements amongst its users.

### 2. Member Definition and Responsibilities <a href="#user-content-2-member-definition-and-responsibilities" id="user-content-2-member-definition-and-responsibilities"></a>

Any person or entity who uses the Libre Blockchain to store, exchange, or process value or information shall be herein referred to as a "Member." Each Member agrees to be bound exclusively by the terms of this Agreement or the most current Libre Governance Documents ratified or amended in accordance with the terms herein, and to select the terms of the current Agreement at the time of the transaction as binding in the interactions between them regarding any and all value and information stored on the Libre Blockchain. It is the responsibility of each Member to abide by their local terrestrial laws and statutes.

### 3. Recording of Values and Information <a href="#user-content-3-recording-of-values-and-information" id="user-content-3-recording-of-values-and-information"></a>

The Libre Blockchain shall be used to record value and information and to perform computations pertaining to these. This information shall be recorded in a continuous chain of blocks of transactions and cryptographic security information ("Blocks") which are tested and validated by computer nodes on the network ("Validating Nodes").

### 4. Accounts <a href="#user-content-4-accounts" id="user-content-4-accounts"></a>

A Member may control one or more accounts on the blockchain. Accounts record the ownership of various coins, tokens, and resources and are controlled by cryptographic keys. Accounts may publish smart contracts including contracts that issue new tokens.

### 5. Native Unit of Value and Voting Power <a href="#user-content-5-native-unit-of-value-and-voting-power" id="user-content-5-native-unit-of-value-and-voting-power"></a>

The native unit of value on the Libre Blockchain shall be the LIBRE coin. Each LIBRE coin can be staked to acquire voting rights. These rights will be measured by a numerical amount "Voting Power", a calculated value defined as a product of the number of LIBRE coins staked by an account plus that number multiplied by the length of staking, in days, as a percentage of days in a year so that coins staked for 365 days or more will receive a total of 200% of the value of coins staked as Voting Power (i.e. the maximum voting coefficient is 2 no matter how many days over 365 the coins may be staked). (Calculated as follows: N \* (1 + (D/365)) where N is the number of LIBRE coins staked and D is days up to a maximum of 365.) Voting Power is recorded in the "votingpower" table of the "stake.libre" smart contract. Voting power is used in voting for DAO Proposals and the election of Block Producers. LIBRE coins may also be used for other purposes including, purchasing additional resources.

### 6. Operation and Execution of the Blockchain <a href="#user-content-6-operation-and-execution-of-the-blockchain" id="user-content-6-operation-and-execution-of-the-blockchain"></a>

The Libre Blockchain reaches consensus regarding the values and transactions it records using delegated proof of stake, whereby active Validating Nodes ("Block Producers") and standby Validating Nodes ("Standby Block Producers") are elected by the votes of the LIBRE coin stakers, as measured by their Voting Power to serve as delegates in the processing and validating of transactions and blocks in the blockchain according the technical specifications of the Libre branch of the Antelope blockchain protocol software.

### 7. Modifying the Blockchain <a href="#user-content-7-modifying-the-blockchain" id="user-content-7-modifying-the-blockchain"></a>

Transactions on the Libre Blockchain shall be reversible only until the Block Producers reach final consensus regarding the validity of the block containing them. Once more than two-thirds of the Block Producers have proposed a block to be valid and an additional Block Producer has accepted the proposed valid block, then it shall be deemed an "Irrevocable Block." The Libre Blockchain shall never amend any Irrevocable Block. Modifications to the blockchain may only be made through rejecting blocks not yet accepted as Irrevocable Blocks and through appending new information into current blocks.

### 8. Block Producer Nomination, Qualification, and Election <a href="#user-content-8-block-producer-nomination-qualification-and-election" id="user-content-8-block-producer-nomination-qualification-and-election"></a>

Any Libre Member may self-nominate as a candidate to be a Block Producer by executing the "regproducer" action of the "eosio" contract and accepting its human-language terms as a binding contract. The Libre Blockchain maintains a list of minimum requirements for acting as a Block Producer or Standby Block Producers (the "Block Producer Minimum Requirements"). The 21 Block Producer candidates currently in compliance with the Block Producer Minimum Requirements receiving the highest weight of Member votes shall serve as Block Producers. The 6 block producer candidates currently in compliance with the Block Producer Minimum Requirements and not serving as Block Producers receiving the highest weight of Member votes shall serve as Standby Block Producers. Enforcement of compliance with the Block Producer Minimum Requirements shall be by a 2/3+1 vote of the Block Producers.

### 9. Block Producers Responsibilities <a href="#user-content-9-block-producers-responsibilities" id="user-content-9-block-producers-responsibilities"></a>

Block Producers shall operate computer infrastructure in accordance with the Libre Block Producer Minimum Requirements. Block Producers are to produce blocks at their scheduled time in accordance with the human-language terms of the "regproducer" action of the "eosio" contract. As the executive authority of the Libre Blockchain, the Block Producers, in aggregate, have broad executive powers over the Libre Blockchain. They may pause the Libre Blockchain, implement new software updates, mint new coins, mint new tokens, implement additional operational rules for the blockchain that do not violate the Agreement, and adjudicate and/or assign cases for arbitration and enforce disputes at their sole discretion by voting for any such action with a majority vote of greater than two-thirds of the Block Producers at any given time.

### 10. Block Producers Responsibility to Libre DAO <a href="#user-content-10-block-producers-responsibility-to-libre-dao" id="user-content-10-block-producers-responsibility-to-libre-dao"></a>

All DAO proposals that reach the state of "Passed Proposal" must be "approved" or "rejected" by 2/3+1 vote of the active Block Producers using the "approve" or "reject" actions in the "dao.libre" smart contract. In this way, the Block Producers have a veto power on any Libre DAO proposal.

Block Producers understand that as validators of the blockchain, they are elected by LIBRE Members who are staking LIBRE and are expected to uphold the will of these Members as expressed by DAO Proposal voting, where those proposals are not in conflict with the then current provisions of the LBOA and its sub-agreements, with the understanding, however, that Block Producers are expected to generally have a deeper understanding of the technical risks and considerations of implementing that will expressed through the DAO proposal system. The terms of the LBOA and its sub-agreements shall supercede the terms of any Passed Proposal, except that modifications shall not be construed as being out of compliance with the LBOA solely by virtue of altering the text of the governance documents themselves. Block Producers shall, therefore, take into consideration any DAO proposals that require their action in order to be completed: including but not limited to smart contract changes, multisig proposals, minting new coins, or anything that the LIBRE stakers propose and vote on. Ultimately, it is within the power of each Block Producer to decide whether or not to implement or sign a multisig to enact all or part of a Passed Proposal. Block Producers are encouraged, but not required, to take into consideration the total percentage of voting power associated with any given proposal.

### 11. Freezing Accounts <a href="#user-content-11-freezing-accounts" id="user-content-11-freezing-accounts"></a>

The Block Producers may issue an emergency order to freeze all transactions from a Member account or accounts as an interim measure which preserves the status quo and preserves assets while a claim is being investigated and decided. The Block Producers do not have the obligation to freeze account transactions. All decisions will be at the sole discretion of the set of Block Producers currently in operation based on the schedule produced by the then operating protocol software. The Block Producers may reassign or nullify the controlling cryptographic keys of the account or accounts.

### 12. Voting <a href="#user-content-12-voting" id="user-content-12-voting"></a>

Libre Members may vote to elect block producer candidates as described in this Agreement. Each Member may vote the entire number of Voting Power in any account they control via private keys. Members may vote for up to 4 block producer candidates at any time and each vote for any candidate will share the same weight as that of all the other candidates. The Block Producers may enact global rules to modify the voting weight based on the age of the votes, the number of votes cast by an account or any other method they deem appropriate by deploying an update of the Libre software designed for this purpose.

### 13. Proxies <a href="#user-content-13-proxies" id="user-content-13-proxies"></a>

Libre does not have the concept of "voting properties" present in other deployments of Antelope software. The Block Producers may modify or add voting proxies at any time. If added, Members may delegate their votes to another Member who has registered as a voting proxy ("Proxy") using the "proxy" contract included in the Antelope software. Only a LIBRE coin’s true beneficial owner or a voting Proxy recorded on the blockchain may vote those coins. No Block Producer or block producer candidate shall operate a Proxy either directly or by any owner or employee of the block producer or candidate. Any Member holding LIBRE coins in trust for another beneficial owner, such as a centralized exchange, may not operate a Proxy, cast votes for or assign to a Proxy such coins. The Block Producers have the authority to remove the proxy power of any Proxy in violation of these rules.

### 14. Initial LIBRE Allocation <a href="#user-content-14-initial-libre-allocation" id="user-content-14-initial-libre-allocation"></a>

The initial distribution of LIBRE coins on the Libre Blockchain ("Libre Initial Distribution") will be determined as follows: 1) Prior to the creation of the Libre Blockchain, no LIBRE coins shall exist and no promise of future LIBRE coins shall be made by any Member. 2) Upon the creation of the Libre Blockchain (on or about July 4, 2022), Block Producers shall earn a block reward for each block they produce that is incorporated into the permanent blockchain record. 3) Standby Block Producers are eligible to earn 20% of the average Block rewards that would be earned by the active Block Producers to be paid by the DAO Fund monthly upon approval by the Block Producers. For clarity, any block producer candidate serving as a Standby Block Producer will earn a reward equivalent to 20% of the Block Producer per-block reward for twelve blocks per executed round of the Block Producer schedule. 4) Block Producer block rewards shall initially be 4 LIBRE coins per block and that amount shall be reduced by half at exactly six months after the creation of the blockchain to 2 LIBRE coins per block, and halved again exactly twelve months after the creation of the blockchain to 1 LIBRE per block. 5) A total of ten million LIBRE coins shall be reserved for free distribution (aka "Airdrop" or "Spindrop") to any claimant who can provide a cryptographic signiture proving ownership of any account with a balance equivalent to one United States of America Dollar in that blockchain's system coin on the EOS, Telos, WAX, Proton, Ethereum or Solana blockchains as of the snapshot date (on or about April 14, 2022). Claimants will receive an amount of LIBRE coins determined by a randomization algorithm for each account claimed and may direct it to any Libre Blockchain account they designate. This free distribution period shall end when the paid distribution (aka "Mint Rush") period begins (on or about December 15, 2022). 6) All LIBRE coins reserved for the Airdrop that remain unclaimed at the end of the claiming period shall be transferred to the Libre DAO Fund account ("dao.libre") for future distribution under the rules of the contract deployed on that account by the Block Producers. 7) The paid distribution will distribute a total of 200 million LIBRE coins in exchange for pNetwork "pegged Bitcoin" (PBTC) tokens over a thirty-day contribution period based on an algorithm that factors the product of a multiplier that declines at an even rate each day of the period plus a second multiplier that grows in a square root fashion for each day the purchaser elects to stake their purchased LIBRE coins from 30 to 365 days. 8) Contributors shall receive their LIBRE coins pro-rata to their contribution divided by total contributions. 9) Contributors also receive a pro-rata distribution of the LIBRE coins allotted to the Mint Rush in the form of Liquidity Pool ("LP") tokens for the PBTC/LIBRE LP, designated BTCLIB tokens. 10) These LP tokens shall be locked in a staking contract ("mint.libre") and a portion shall be unlocked for claiming as liquid BTCLIB tokens daily. 11) The contributor shall be able to withdraw the LIBRE coins purchased along with all staking rewards accrued once the staking period is complete. Alternatively, the purchaser may elect to withdraw LIBRE coins prior to the completion of their staking period by relinquishing all accrued interest and 20% of the principal LIBRE coins purchased.

### 15. Libre DAO Fund <a href="#user-content-15-libre-dao-fund" id="user-content-15-libre-dao-fund"></a>

LIBRE coins shall be collected in a "DAO Fund" account ("dao.libre") for distribution by the votes of Members using the Voting Power algorithm. The intended purpose of the DAO Fund is to provide a source of future funding for any needs the Libre Blockchain may have beyond block rewards, including, without limitation, pay for the development of core software code, user interfaces and applications, marketing and promotion, and memberships in industry organizations.

### 16. Libre DAO Tax <a href="#user-content-16-libre-dao-tax" id="user-content-16-libre-dao-tax"></a>

In order to continuously fund the DAO Fund account, one tenth of any new LIBRE minted as a rewared for each staking LIBRE coin transaction amount shall be deposited as newly minted LIBRE coins into the DAO Fund account as a "DAO Tax" upon the unstaking of the principal.

### 17. Libre DAO Additional Funding <a href="#user-content-17-libre-dao-additional-funding" id="user-content-17-libre-dao-additional-funding"></a>

Any vote by the Libre DAO proposing to increase the LIBRE coin supply must receive at least three times greater voter participation than the minimum amount required to pass a funding or governance amendment proposal without such a coin supply increase, in order to be considered a Passed Proposal to the Block Producers. The Block Producers, in their sole discretion, will have ultimate authority over whether or not to enact changes, in whole or in part, of any Passed Proposal. On March 1, 2023 a DAO proposal shall be proposed to mint exactly one hundred million (100,000,000.0000) new LIBRE coins directly into the DAO Fund account with the express intention and admonishment to future Block Producers that this shall be a singular token creation event to allow funding of ongoing technical development, Member outreach and service, and funding of partnerships to allow the Libre Blockchain to operate optimally for current and future Members.

### 18. Libre DAO Proposal Submission <a href="#user-content-18-libre-dao-proposal-submission" id="user-content-18-libre-dao-proposal-submission"></a>

Any Libre Member or group of Members may submit proposals for the usage LIBRE coins accumulated in the DAO Fund account by executing the required actions to compose and submit a proposal to the "dao.libre" contract (the "Proposer") and providing the required information including, at least, a full description of the proposal, a link to a fixed, permanent source of information, and the exact deposit transaction, including deposit account or accounts, that will occur should the proposal be accepted by the Libre Members. DAO proposals may also recommend modifications of the LBOA and/or its sub-agreements, either with or without an associated funding component.

### 19. Libre DAO Proposal Voting Period <a href="#user-content-19-libre-dao-proposal-voting-period" id="user-content-19-libre-dao-proposal-voting-period"></a>

Once a proposal is submitted, there will be a period of three million blocks (approximately 10 days) in which a proposal may be voted (the "Voting Period"). This period can be changed by the block producers by updating the "params" table on the "dao.libre" smart contract with 2/3+1 consensus.

### 20. Libre DAO Proposal Submission Fee <a href="#user-content-20-libre-dao-proposal-submission-fee" id="user-content-20-libre-dao-proposal-submission-fee"></a>

A non-refundable submission fee of 10,000.0000 LIBRE shall be required as part of each DAO Proposal submission. The Proposer may elect to add the amount of the submission fee in the proposal to be paid if the propoal is accepted. This fee can be changed by the block producers by updating the "params" table on the "dao.libre" smart contract with 2/3+1 consensus.

### 21. Passage of a Libre DAO Proposal <a href="#user-content-21-passage-of-a-libre-dao-proposal" id="user-content-21-passage-of-a-libre-dao-proposal"></a>

Voting on a DAO Proposal shall be tallied using the total amount of Voting Power of all Libre accounts at the time of the closing of the Voting Period. Any Libre DAO Proposal that receives a 51% majority of YES votes and a minimum threshold of 10.0% of total Voting Power from all Voting Power then on the Libre Blockchain at the conclusion of the Voting Period shall be a "Passed Proposal" except proposals that would increase the LIBRE coin supply, which proposals will require three times the minimum voting threshold of a proposal that does not increase the LIBRE coin supply to become a Passed Proposal. This voting period and threshold can be changed by the block producers by updating the "params" table on the "dao.libre" smart contract with 2/3+1 consensus.

### 22. Execution of a Libre DAO Proposal <a href="#user-content-22-execution-of-a-libre-dao-proposal" id="user-content-22-execution-of-a-libre-dao-proposal"></a>

Each Passed Proposal shall have the transaction described in the proposal available to execute immediately by a smart contract action by the active permission of "dao.libre" assigned currently to the Block Producers and requiring 2/3+1 approval, provided sufficient funds exist in the DAO Fund account.

### 23. Failure to Provide Libre DAO Proposal Deliverables <a href="#user-content-23-failure-to-provide-libre-dao-proposal-deliverables" id="user-content-23-failure-to-provide-libre-dao-proposal-deliverables"></a>

The Block Producers may, by a 2/3+1 concensus majority vote, elect to reclaim some or all of the disbursed funds from an Passed Proposal that has been executed, but where the proposed work appears not to have been performed as described in the Proposal. Block Producers may also pay out funds due a Passed Proposal in tranches aligned with delivery milestones in the sole discretion of the Block Producers. Any funds reclaimed or not distributed to the recipients as described in the DAO Proposal or later amended by mutual agreement of the Proposer and the Block Producers after one year from the end of the Voting Period shall be returned to the Libre DAO account.

### 24. Recording Governance Amendments to the Blockchain <a href="#user-content-24-recording-governance-amendments-to-the-blockchain" id="user-content-24-recording-governance-amendments-to-the-blockchain"></a>

The official governance documents of the Libre Blockchain shall be written to the blockchain by the "Libre Governance Account" ("gov.libre") as plain text in Markdown file format available for any Member to consult. The Libre Governance Account shall be controlled via a multisig of the Block Producers by a 2/3+1 majority consensus. The first recording to the Libre Governance Account shall be adopted by a vote of the Block Producers. A DAO Proposal to allow Members to ratify the initial recording may occur but is not required for the documents adopted by the Block Producers to be valid and in effect, and any DAO Proposal to ratify that does not become a Passed Proposal at the end of its Voting Period does not invalidate the governance writings already adopted by the Block Producers.

### 25. No Fiduciary, Partnership or Duty to Create Profit <a href="#user-content-25-no-fiduciary-partnership-or-duty-to-create-profit" id="user-content-25-no-fiduciary-partnership-or-duty-to-create-profit"></a>

No Member shall have fiduciary responsibility to support the value of the LIBRE coin. The Members do not authorize anyone to hold assets, borrow, nor contract on behalf of the Members collectively. The Libre Blockchain Network has no owners, managers nor fiduciaries. No Member intends or claims partnership with any other Member on the basis of this Agreement. Each Member is an individual person or entity and no general or limited partnership is created or joined by engaging with the Libre Blockchain. No Libre Blockchain Block Producer, Standby Block Producer, developer nor any other Member is charged with a duty nor expectation to support or contribute to the market price of LIBRE coins or in any other way create a profit for holders of the LIBRE coin. All members agree that the market price of LIBRE coins is a function of laissez-faire market forces beyond the control of any Member. Every Member expressly disavows any expectation of profit from the contribution of any other Member or Members, whatever role they may undertake in the operations or development of the Libre Blockchain.

### 26. Publication to the Libre Blockchain <a href="#user-content-26-publication-to-the-libre-blockchain" id="user-content-26-publication-to-the-libre-blockchain"></a>

Each Member grants all other Members an irrevocable license to view transactions as recorded and published to the blockchain with ownership of the exact text published on the chain considered to be in the public domain.

### 27. Responsibility for Information <a href="#user-content-27-responsibility-for-information" id="user-content-27-responsibility-for-information"></a>

Members are advised that all data posted to the Libre Blockchain is intended to be stored immutably for as long as computers may exist. The Libre Blockchain, its Block Producers, developers and Members shall bear no responsibility or enforcement role for any information, data, or content improperly recorded on the blockchain. Any penalties or Decisions for improperly recorded information, data, or content shall be the sole responsibility of the Member that posted it.

### 27. Restitution <a href="#user-content-27-restitution" id="user-content-27-restitution"></a>

Each Member agrees that penalties for breach of this contract may include, but are not limited to, fines, loss of account, and any other measures decided by Block Producers as defined in this Agreement. No Block Producer shall be deemed liable for restitution for any action performed as a Block Producer in compliance with the pertinent terms of the most recent governance document recorded at the Libre Governance Account at the time of the action, which actions are to be interpreted with all benefit of ambiguity, lack of clarity and silence on any matter to accrue to the Block Producer.

### 28. Amending <a href="#user-content-28-amending" id="user-content-28-amending"></a>

Any amendment to the Libre governance documents recorded at the Libre Governance Account will require a completed DAO Proposal vote meeting the qualifications of a Passed Proposal at the end of its Voting Period. The Block Producers must also approve the proposal or a verion of it that the Block Producers modify, in order to record it to the blockchain from the Libre Governance Account. Documents must be recorded in their entirety and complete with all sub-agreements. The document most recently recorded shall be deemed the full, complete and duly adopted governance writings of the Libre Blockchain and all previously adopted versions shall be null and void.

### 29. Severability <a href="#user-content-29-severability" id="user-content-29-severability"></a>

If any part of this Agreement is declared unenforceable or invalid, the remainder will continue to be valid and enforceable. No part of this Agreement is to be given higher importance than any other due to its ordinal position within the document. Paragraph titles are intended for reference purposes only and shall not be interpreted to modify the meaning of the text of the paragraph.

### 30. Termination <a href="#user-content-30-termination" id="user-content-30-termination"></a>

A Member is automatically released from all revocable obligations under this Agreement five years after the Member has sold or otherwise relinquished all LIBRE coins.

### 31. Grammar <a href="#user-content-31-grammar" id="user-content-31-grammar"></a>

Any noun in this document used in the singular form shall also apply in the plural form and vice versa. Likewise, any masculine, feminine or neutral reference shall apply to every gender or neutral tense.

### 32. Consideration <a href="#user-content-32-consideration" id="user-content-32-consideration"></a>

All rights and obligations under this Agreement are mutual and reciprocal and of equally significant value and cost to all parties.

### 33. Acceptance <a href="#user-content-33-acceptance" id="user-content-33-acceptance"></a>

This Operating Agreement and all sub-agreements constituting the most recently recorded document on the Libre Governance Account is deemed fully and irrevocably accepted when a Member signs any transaction on the Libre Blockchain which incorporates a Transaction as Proof of Stake (TAPOS) cryptographic proof of an action within a block whose implied state incorporates an Application Binary Interface (ABI) of said contract and said transaction is incorporated into the blockchain. It is expressly understood that acceptance shall apply retroactively to all coins, tokens or other representations of value that may have been recorded on a Member's Libre account prior to the Member's first blockchain action signifying acceptance.

### 34. Counterparts <a href="#user-content-34-counterparts" id="user-content-34-counterparts"></a>

This Agreement may be executed in any number of counterparts, each of which when executed and delivered shall constitute a duplicate original, but all counterparts together shall constitute a single agreement.

### 35. Complete Agreement <a href="#user-content-35-complete-agreement" id="user-content-35-complete-agreement"></a>

This Agreement is accepted as complete and needs no further ratification to be valid and enforceable.
