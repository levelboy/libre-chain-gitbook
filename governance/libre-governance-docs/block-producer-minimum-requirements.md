---
description: >-
  https://gitlab.com/libre-chain/libre-governance/-/blob/main/block-producer-minimum-requirements.md
---

# Block Producer Minimum Requirements

## Libre Blockchain Block Producer Minimum Requirements:

The intent of this document is to clarify the minimum requirements for Block Producers to participate in the Libre Blockchain. This document is an addendum to the Libre Blockchain Operating Agreement and Regproducer Agreement and inherits their terms and language. The specifications stated herein are subject to revision by the Libre Block Producers as determined by a multi-signature action of 2/3+1 of the current Block Producersat any time. Regardless of their level of participation, to participate in the Libre Blockchain, Block Producer candidates are required to provide, and abide by the following:

### A. Disclosures: <a href="#user-content-a-disclosures" id="user-content-a-disclosures"></a>

#### 1. Block Producer Account Name <a href="#user-content-1-block-producer-account-name" id="user-content-1-block-producer-account-name"></a>

#### 2. Block Producer Block-Signing Public Key <a href="#user-content-2-block-producer-block-signing-public-key" id="user-content-2-block-producer-block-signing-public-key"></a>

#### 3. Block Producer Organization Info: <a href="#user-content-3-block-producer-organization-info" id="user-content-3-block-producer-organization-info"></a>

a. Candidate Name

b. Candidate Website URL

c. Candidate country of registration for registered entity or residence of primary owner if not a registered entity as 2-letter ISO country coded.

d. Candidate server location(s)

i. Location name

ii. Country as 2-letter country code

iii. Latitude

iv. Longitude

v. Geographically-based numeric code in compliance with the block producer schedule optimizing structure implemented in the Libre eosio system contract.

#### 4: Network Emergency Contact(s) <a href="#user-content-4-network-emergency-contacts" id="user-content-4-network-emergency-contacts"></a>

For each owner and lead system administrator (if different):

a. Full name

b. Email address

c. Mobile phone number - in a non-public, password protected, repository commonly accessible to any of the paid Block Producers and Standby Block Producers.

d. Telegram handle

### B. Practices: <a href="#user-content-b-practices" id="user-content-b-practices"></a>

1. Sync with a mutually approved network time protocol (NTP) server at least once per 24 hours.
2. Monitor and maintain your Producer node(s) and Public endpoint(s) in good working order.

### C. Minimum Technical Requirements <a href="#user-content-c-minimum-technical-requirements" id="user-content-c-minimum-technical-requirements"></a>

#### 1. Public Endpoints: <a href="#user-content-1-public-endpoints" id="user-content-1-public-endpoints"></a>

**a. Testnet:**

**i. P2P endpoint (Minimum of 1)**

**Technical Requirements:**

* Full block history from genesis.
* Synced within 1200 blocks of head block.
* May be combined with Testnet Producer Node and Public API endpoint.

**ii. HTTP & HTTPS API endpoint (Minimum of 1)**

**Technical Requirements:**

* Minimum RAM (per node): 16GB
* Minimum Disk (per node): 250GB
* Firewall: Active
* Plugins: chain, chain\_api
* May be combined with Testnet Producer Node and Public P2P endpoint.

**b. Mainnet:**

**i. P2P endpoint (Minimum of 1)**

**Technical Requirements:**

* Full block history from genesis.
* Synced within 600 blocks of head block.
* May be combined with Mainnet Public API endpoint.

**ii. HTTP & HTTPS API endpoint (Minimum of 1)**

**Technical Requirements:**

* Minimum RAM (per node): 32GB
* Minimum Disk (per node): 250GB
* Firewall: Active
* Plugins: chain, chain\_api
* May be combined with Mainnet Public P2P endpoint.

#### 2. Producer Nodes: <a href="#user-content-2-producer-nodes" id="user-content-2-producer-nodes"></a>

**a. Testnet:**

**i. Producer Node (Minimum of 1)**

**Technical Requirements:**

* Minimum RAM (per node): 16GB
* Minimum Disk (per node): 250GB
* Minimum Processor: Ability to produce blocks in less than 350ms
* Firewall: Active
* Plugins: chain, producer
* The Over-clocking setting shall not be used on any block producer nodes.
* May be combined with Testnet Public P2P and API endpoints.

**Performance Requirements:**

* A minimum of 1,200,000 blocks (approximately 7 days) of synched and registered operation of a Libre testnet block production node measured from the first block produced.
* Ongoing operation of a synched, registered, and publicly accessible Libre testnet block production node within the past 500,000 blocks (approximately 70 hours).
* A maximum total of 1,000,000 blocks of missed production or blocks dropped by network within the most recent 5,000,000 blocks (approximately 29 days). Enforcement of which commences at Libre mainnet block height 5,000,000.
* Resolution of downtime events within 4 hours of first missed round.
* Failure to maintain the prescribed block production uptime or dowtime resolution time will result in a kickbp multi-signature action to remove the Block Producer from service for 168 hours (7 days).
* Producers who are removed from the schedule by a kickbp multi-signature action more than 3 times in a rolling 365 period will have their regprod permission removed.

**b. Mainnet:**

**i. Producer Node (Minimum of 1)**

**Technical Requirements:**

* Minimum RAM (per node): 32GB
* Minimum Disk (per node): 250GB
* Minimum Processor: Ability to produce blocks in less than 250ms
* Firewall: Active
* Plugins: chain, producer
* The Over-clocking setting shall not be used on any block producer nodes.
* No publicly available services running on Producer Node.

**Performance Requirements:**

* A maximum of 2% of the Block Producer's scheduled blocks either missed or dropped by network within the most recent 5,000,000 blocks (approximately 29 days). Enforcement of which commences at Libre mainnet block height 5,000,000.
* Resolution of downtime events within 4 hours of first missed round.
* Failure to maintain the prescribed block production uptime, or dowtime resolution time, will result in a kickbp multi-signature action to remove the Block Producer from service for 168 hours (7 days).
* Producers who are removed from the schedule by a kickbp multi-signature action more than 3 times in a rolling 365 period will have their regprod permission removed.

## Copyright: <a href="#user-content-copyright" id="user-content-copyright"></a>

This document is in the public domain.
