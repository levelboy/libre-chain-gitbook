---
description: Libre
---

# Libre Lightning Provider

Libre uses the concept of Libre Lightning Providers (LLPs) to pay and receive BTC. Each of these Lightning Service providers runs its own Lightning node, and has an address on the Libre network where it can send and receive pegged Bitcoin ([PBTC](../cross-chain-interoperability/bitcoin-mainnet.md)). See also [bitcoin-lightning-network.md](../cross-chain-interoperability/bitcoin-lightning-network.md "mention")

### Lightning Node Operators

For those who have not set up a Lightning node before, we will soon have an easy method for you to set up your own node and connect to Libre.

At this time, the best way to earn fees as an LLP is to just peer with our existing nodes. Soon, we will be looking to expand this through partnerships and opening up our Lightning txs to a market of nodes.

To become an LLP is a permission-less process - just peer with one of our LND nodes and keep your channels balanced:

`03e036c7da199f4dd1f2ea0eaedf2ba6142850553671cdc7d502ec382173ba9396`

`03a84351bb64b77d1bc69ca69fe8fd2a8a14bc4f8e10373e8e3d735e614420ffa1`\


### Getting Started with Lightning

You can run your own Lightning node like a pro in 16 steps.

Sign up for [Voltage](https://voltage.cloud/) or get [hardware and install lnd](https://github.com/bensig/bitcoin-lightning-notes/blob/main/index.md)

1. Initialize the node&#x20;
2. Deposit some BTC into your node
3. Backup the seed phrase - SOMEWHERE SAFE OR YOU WILL LOSE YOUR BTC
4. Join some [Telegram channels for noderunners](https://github.com/bensig/bitcoin-lightning-notes/blob/main/index.md#lightning-noderunner-groups)
5. Open 1 channel to another node (you can use [Flow](https://voltage.cloud/flow/))
6. Backup the channels
7. Now go to [https://terminal.lightning.engineering](https://terminal.lightning.engineering) and connect your node
8. Get the recommended nodes to connect to
9. Create a txt with the pub keys of the nodes amounts to open to - recommend doing 1000000 SATS min and 15000000 max
10. Add the Libre nodes to your list: `03e036c7da199f4dd1f2ea0eaedf2ba6142850553671cdc7d502ec382173ba9396` --amount 1000000  `03a84351bb64b77d1bc69ca69fe8fd2a8a14bc4f8e10373e8e3d735e614420ffa1` --amount 1000000
11. Install “Balance of Satoshis” [https://github.com/alexbosworth/balanceofsatoshis/](https://github.com/alexbosworth/balanceofsatoshis/)
12. Do a `bos open` and feed in that list of channels
13. Backup the channels
14. Now, set the fee rate on your channels - you can use a tool like Suez [https://github.com/prusnak/suez](https://github.com/prusnak/suez) - generally, I would recommend starting with 1000 msats and seeing if you get any tx in the first day
15. Check your tx history for forwards and fees earned daily - adjust fees to be higher where you have less outbound capacity
16. Use `bos rebalance` to keep your channels balanced
