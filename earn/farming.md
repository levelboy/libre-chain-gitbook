---
description: Farming on Libre Chain
---

# Farming

Anyone can provide liquidity to the Libre AMM. As an incentive, they will be able to earn LIBRE rewards by staking their LP tokens. For example, the PUSDT / PBTC pool will accrue farming rewards equal to 1/4 of whatever validators earn per day.

To add liquidity, visit [https://dashboard.libre.org/swap](https://dashboard.libre.org/swap)&#x20;

Farming APYs will depend on the number of participants farming each pool and the share of staked LP tokens that each account has staked.

## Calculating Farming Rewards

Farming rewards are emitted on every block that is produced. On Libre, a block is currently produced every 0.5 seconds which means there are 2 blocks per second, then we can calculate the rewards per day using this:&#x20;

```
total_libre_rewarded = reward_per_block * 2 blocks * 60 sec * 60 min * 24 hours
```

The reward per block is set in the reward.libre::global table. As of April 12, 2023 - this is set to 1 LIBRE / block. So the daily emission can be calculated below:

```
total_libre_rewarded = 1.0000 LIBRE * 2 * 60 * 60 * 24 = 172800.0000 LIBRE
```

This total reward is divided by the number of "allowed\_pools" set in the reward.libre::global table.

```
total_libre_per_pool = total_libre_rewarded / allowed_pools
```

Currently, this is 2 pools - BTCUSD and BTCLIB, so we can calculate the total reward per pool as follows:

```
172800.0000 / 2 = 86400.0000 LIBRE
```

This amount is paid pro-rata daily to each pool. The value of your percentage of the pool as a ratio to the value of the reward is how the APY is calculated.&#x20;

Details are in the [open-source reward.libre smart contract](https://gitlab.com/libre-chain/staking-contract/-/tree/testnet/reward). Click below to see the code.

{% embed url="https://gitlab.com/libre-chain/staking-contract/-/tree/testnet/reward" %}
