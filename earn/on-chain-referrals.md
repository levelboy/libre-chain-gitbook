---
description: Libe on-chain referral system.
---

# Referrals

The Libre blockchain is the first blockchain in the world (we think) that was built with the idea of on-chain referrals. The way it works is simple:\
\
Every account (example: bob) can refer multiple other accounts (example: alice and susan). This parent-child relationship is stored in a table on the actual chain. Unlike humans, each account has a single parent. Going back up a level, alice might be referred to by bob, but bob was referred by mike. \
\
Now when alice joins the system, and earns some tokens -- bob will make a % on top of the rewards.

Any app that builds on Libre is able to take advantage of the referral system to incentivize affiliates.
