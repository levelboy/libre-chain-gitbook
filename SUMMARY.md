# Table of contents

* [Guide to Libre](README.md)
  * [Libre Level 1 - Beginner](guide-to-libre/libre-level-1-beginner.md)
  * [Libre Level 2 - Advanced](guide-to-libre/libre-level-2-advanced.md)

## The Libre Blockchain

* [Founding Principles](<README (1).md>)
  * [Freedom](the-libre-blockchain/what-is-libre/freedom.md)
  * [Usability](the-libre-blockchain/what-is-libre/usability.md)
* [Validators](the-libre-blockchain/validators.md)
* [Building on Libre](the-libre-blockchain/building-on-libre/README.md)
  * [Building Smart Contracts on Libre](the-libre-blockchain/building-on-libre/building-smart-contracts-on-libre.md)
  * [Libre Developer Tools](the-libre-blockchain/building-on-libre/libre-developer-tools/README.md)
    * [Libre Testnet](the-libre-blockchain/building-on-libre/libre-developer-tools/libre-testnet.md)
  * [API Docs](the-libre-blockchain/building-on-libre/api-docs.md)

## The LIBRE Coin

* [Supply](the-libre-coin/supply.md)
* [Mint Rush](the-libre-coin/mint-rush.md)
* [Spindrop](the-libre-coin/spindrop/README.md)
  * [How to Claim the LIBRE Spindrop](the-libre-coin/spindrop/how-to-claim-the-libre-spindrop.md)
  * [Eligibility Requirements for the LIBRE Spindrop](the-libre-coin/spindrop/eligibility-requirements-for-the-libre-spindrop.md)

## ACCOUNTS AND WALLETS

* [LIBRE Account Names](accounts-and-wallets/libre-account-names/README.md)
  * [Cryptographic Keys](accounts-and-wallets/libre-account-names/cryptographic-keys.md)
* [Creating Multiple Accounts](accounts-and-wallets/creating-multiple-accounts.md)
* [Bitcoin Libre Wallet](accounts-and-wallets/bitcoin-libre-wallet.md)
* [Anchor Wallet](accounts-and-wallets/anchor-wallet/README.md)
  * [Creating a Libre MainNet Account in Anchor](accounts-and-wallets/anchor-wallet/creating-a-libre-mainnet-account-in-anchor.md)
  * [Creating a Libre Testnet Account in Anchor](accounts-and-wallets/anchor-wallet/creating-a-libre-testnet-account-in-anchor.md)
  * [Getting Testnet LIBRE tokens](accounts-and-wallets/anchor-wallet/getting-testnet-libre-tokens.md)
* [Ledger Hardware Wallet](accounts-and-wallets/ledger-hardware-wallet.md)

## EARN

* [Staking](earn/staking.md)
* [Referrals](earn/on-chain-referrals.md)
* [Farming](earn/farming.md)
* [Become a Validator](earn/become-a-validator.md)
* [DAO Tax](earn/dao-tax.md)
* [Libre Lightning Provider](earn/libre-lightning-provider.md)
* [Play To Earn](earn/play-to-earn.md)

## Governance

* [Voting Power](governance/voting-power.md)
* [Validator Election](governance/validator-election.md)
* [Libre DAO](governance/dao.md)
* [Libre Governance Docs](governance/libre-governance-docs/README.md)
  * [Libre Blockchain Operating Agreement (LBOA)](governance/libre-governance-docs/libre-blockchain-operating-agreement-lboa.md)
  * [Block Producer Agreement](governance/libre-governance-docs/block-producer-agreement.md)
  * [Block Producer Minimum Requirements](governance/libre-governance-docs/block-producer-minimum-requirements.md)
* [Libre Mainnet Multisig Gov](governance/libre-mainnet-code-update-proposals/README.md)
  * [April 2023](governance/libre-mainnet-code-update-proposals/april-2023.md)
  * [March 2023](governance/libre-mainnet-code-update-proposals/march-2023.md)
  * [February 2023](governance/libre-mainnet-code-update-proposals/feb23stake.md)
  * [December 2022](governance/libre-mainnet-code-update-proposals/dec22updates.md)

## Technical Details

* [Libre Core Constants](technical-details/libre-core-constants.md)
* [Mint Rush Details](technical-details/mint-rush-details.md)
* [Staking Details](technical-details/staking-details.md)
* [Verifying Code Updates](technical-details/verifying-code-updates.md)
  * [Deployed Smart Contract Versions](technical-details/deployed-smart-contract-versions.md)

## Cross-Chain Interoperability

* [Ordinals (wrapped)](cross-chain-interoperability/ordinals-wrapped.md)
* [Bitcoin Lightning Network](cross-chain-interoperability/bitcoin-lightning-network.md)
* [Bitcoin Mainnet](cross-chain-interoperability/bitcoin-mainnet.md)
* [USDT / Ethereum](cross-chain-interoperability/usdt-ethereum.md)
* [pNetwork](cross-chain-interoperability/pnetwork.md)
