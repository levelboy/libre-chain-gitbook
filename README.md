---
description: Welcome to Libre, we're excited that you're here.
---

# Guide to Libre

<mark style="background-color:orange;">Start here</mark><mark style="background-color:orange;">**:**</mark>

[Libre Level 1 - Beginner](guide-to-libre/libre-level-1-beginner.md)

[Libre Level 2 - Advanced](guide-to-libre/libre-level-2-advanced.md)

What is Libre?

\


THE NUMBERS&#x20;

500x Faster than Bitcoin

4000 transactions per second

0.05s Transaction Speed

0.01% Ecological Impact&#x20;

$0 Transaction Costs

\


THE TRIFECTA

Scalable, Decentralized, Secure

\


HOW LIBRE DOES IT&#x20;

Pegged Assets (Bitcoin, Tether, Ordinals, BRC20)

Sidechain (scaling layer) for the Bitcoin Blockchain

Simple-Name Addresses with Multi-Sig & Advanced Permissions

pNetwork - Secure Interoperability between Chains

Anteliope.io - Fast, Secure, User-Friendly Web3 Products

The Lightning Network - Smart Contract Function for Instant Payments

Delegated Proof-of-Stake - Evolution in Energy Efficiency&#x20;

Fair Launch - Free of Outside Funding or Concentration of Ownership

\


CURRENTLY LIBRE IS -&#x20;

Bridging/Connecting Bitcoin and Ethereum in our Superfast Wallet&#x20;

Building the First Ordinals Marketplace featuring Wrapped Ordinals (also superfast)

Join us in Libre Cafe for Basic Education. Informal, Inclusive.&#x20;

\


CURRENT PROBLEMS WE ARE SOLVING -&#x20;

Scaling Bitcoin by making transactions fast and free within our 2nd Layer (Sidechain).&#x20;

Making Ordinals Fast, Easy and Programmable.

Creating Easy-to-Use Products that are intuitive and easily adoptable.&#x20;

\
