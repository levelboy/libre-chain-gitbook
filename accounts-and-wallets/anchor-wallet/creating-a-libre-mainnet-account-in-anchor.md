---
description: Everything you need to know about the Greymass Anchor desktop wallet.
---

# Creating a Libre MainNet Account in Anchor

Anchor is the leading desktop wallet for all Antelope chains. It is extremely reliable and trustworthy. You can download it at [https://greymass.com/en/anchor/](https://greymass.com/en/anchor/)\
\
Anchor can be used with LIbre in two ways\
\
\- By importing an existing Libre account using a 51 char alphanumeric private key (beginning with a 5)\
\- or by connecting to a Ledger Hardware Wallet.\
\
We'll cover the Ledger setup later. For now, lets focus on importing a key. For this, these are the steps\
\
1\. Generate a private key and 12 word seed phrase from the Ian Colman Utility or any other EOSIO key generator.  [https://github.com/iancoleman/bip39](https://github.com/iancoleman/bip39). Save BOTH the seed phrase and key.\
\
2\. Insert that private key into the Libre account generator: [https://accounts.libre.org/](https://accounts.libre.org/)\
\
3\. Import the private key into Anchor.\
\
4\. Add the token addresses for pBTC and pUSDT as follows\
\
contract: BTC.ptokens\
token: pBTC\
\
contract: USDT.ptokens\
token: pUSDT\
\
5\. At this point, you can see your balances in Anchor like this\
\


<figure><img src="../../.gitbook/assets/Screen Shot 2022-11-06 at 12.28.39 PM.png" alt=""><figcaption></figcaption></figure>

As long as you have saved the seed phrase, you will be able to import the account in Bitcoin Libre as well.
