---
description: Using Bitcoin Libre with Libre chain.
---

# The Bitcoin Libre Wallet

The Bitcoin Libre wallet was at the genesis of the Libre blockchain and can be viewed as one of the "official" wallets of Libre. The wallet is owned and maintained by an independent company however, and **is not needed to access the features of the LIBRE blockchain**. It is critical that the operation of the blockchain not depend on any user interface, in fact the entire full features of **the chain can be accessed at the command line level with the Cleos wallet**.\
\
At the end of the day, Bitcoin Libre has several core features, and no more or less than other wallets:\
\
\- can send tokens to another Libre account name\
\- can query the blockchain history nodes and get account balances\
\- can sign in and verify account ownership (supports Libre signing request)\
\
In addition, the Bitcoin Libre wallet currently has a daily spin wheel which will be subsidized (as will other free to play apps) by the DAO.

[https://www.bitcoinlibre.io/](https://www.bitcoinlibre.io/)
