---
description: How to create (multiple) accounts and manage them on Libre blockchain.
---

# Creating Multiple Accounts

**1. Go to Ian Coleman’s tool.** [**https://iancoleman.io/bip39/**\
\
](https://iancoleman.io/bip39/)**2. Generate 12 word seed phrase**\
\
`venture cat holiday scatter plunge choose bacon elephant stadium acquire dice alarm`\
\
**3. Click Coin EOS: (Picks BIP 44 Derivation Path)**\
\
Copy Public and Private keys\
\
`EOS572k6vM7dWQ9aon47RChjF6rosKb9Jjm2NK27Nb7ZV3eFWff9c`\
`5JYaSpTXi6WwNRWKUb6aNrsRMnu595dbtFudFFCajmx25FDBMEg`\
\
**4. Save seed phrase, public key, private key**\
\
You will potentially need all three of these, but especially the seed phrase for Bitcoin Libre and the the private key for Anchor. You will also need the public key if you want to re-use the same private key on a different account name\
\
**5. Create an account using the Libre web tool** [**https://accounts.libre.org/**\
\
](https://accounts.libre.org/)You will need the public key and you can choose any 12 letter account name.\
\
Now, you can easily create as many accounts as you want using these 5 steps. For each account, use either Bitcoin Libre or Anchor and use the dashboard to login. The accounts can be funded with Bitcoin using the receive address on the dashboard or mobile app.\
\
Advantages of this versus single account on Bitcoin Libre:\
\
\- can move the same account between Anchor and Bitcoin Libre\
\- can pick the seed phrase using external mechanism (Ian Coleman’s tool is just one example)\
\- seed phrase can be done offline for max security\
\- can potentially move an account to a third party by swapping keys later on (staking stays the same)\
\- better to have lots of small accounts vs one big one (again, security). Anchor is good for this.

\
