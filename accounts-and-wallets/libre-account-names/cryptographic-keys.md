---
description: >-
  There are many ways to generate a cryptographic key for securing your Libre
  account.
---

# Cryptographic Keys

This is a more advanced topic on key creation and management.

[.](./ "mention")are protected by cryptographic keys. This is typical for all blockchain accounts. Libre uses the same elliptical curve cryptoraphy as Bitcoin ([secp256k1](https://en.bitcoin.it/wiki/Secp256k1)). To generate your own keys you can use any of the following tools.&#x20;

The benefit of generating your own seed phrase is that you will be able to import your account into Bitcoin Libre which provides a simple to use interface.&#x20;

### **Generate Seed Phrase**

Libre is compatible with any any Standard BIP 39/32/44 seed generator.

You can find [many articles online](https://duckduckgo.com/?q=bip39+12+word+seed+generator\&va=j\&t=hc\&ia=web) about how to generate these or use one of the following tools.

{% hint style="info" %}
The secp256k1 derivation path is 44'/194'/0'/0/0
{% endhint %}

**Bitcoin Libre** (also creates account for you) [https://www.bitcoinlibre.io](https://www.bitcoinlibre.io)

* **Ian Coleman BIP39 tool** (open source)  [https://iancoleman.io/bip39/](https://iancoleman.io/bip39/)
* **Ian Coleman BIP39 tool** **offline** (open source)  [https://github.com/iancoleman/bip39](https://github.com/iancoleman/bip39)
* **Libre key tool (open source) online or offline** [https://github.com/bensig/libre-key-tool](https://github.com/bensig/libre-key-tool)
* **Bitcoin Libre Seed to Key Converter** (open source) [https://github.com/salimcansatici/Bitcoin-Libre-Seed-to-Key-Converter](https://github.com/salimcansatici/Bitcoin-Libre-Seed-to-Key-Converter)

### Extract Key from Seed Phrase

Any of the following tools can also extract a key from any BIP39 12 word seed phrase.&#x20;

* **Ian Coleman BIP39 tool** (open source)  [https://iancoleman.io/bip39/](https://iancoleman.io/bip39/)
* **Ian Coleman BIP39 tool** **offline** (open source)  [https://github.com/iancoleman/bip39](https://github.com/iancoleman/bip39)
* **Libre key tool (open source) online or offline** [https://github.com/bensig/libre-key-tool](https://github.com/bensig/libre-key-tool)
* **Bitcoin Libre Seed to Key Converter** (open source) [https://github.com/salimcansatici/Bitcoin-Libre-Seed-to-Key-Converter](https://github.com/salimcansatici/Bitcoin-Libre-Seed-to-Key-Converter)

### **Generate Key**

Simple and secure ways to generate a key to use with Anchor or cleos wallets.

* [Anchor Wallet](../anchor-wallet/)
* [Ledger](../ledger-hardware-wallet.md) Hardware Wallet

To create a new account using your key - go to [https://accounts.libre.org ](https://accounts.libre.org)

If you created a seed phrase, you can easily import it into [Bitcoin Libre](../bitcoin-libre-wallet.md)
