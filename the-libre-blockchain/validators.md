---
description: Libre Chain validator node information.
---

# Validators

The Libre chain is secured by its decentralized network of validators. These validator nodes are independently owned and located all over the world.

<figure><img src="../.gitbook/assets/image (12).png" alt=""><figcaption><p>Libre mainnet node locations as tracked by antelope.tools (see link below)</p></figcaption></figure>

Libre validator nodes provide APIs for submitting transactions, chain history for wallets and applications, as well as validation of blocks for which validators are paid.&#x20;

Validators are paid for each block on the Libre chain that they produce. There is a halvening schedule for block pay which occurs on a superblock. A superblock happens every 15,768,000 seconds.&#x20;

Initially, validators were paid 4 LIBRE per block and there will be 2 halvenings, one at 6 months and one at 12 months. The block rewards have a terminal rate of 1 LIBRE per block.

You can learn about the existing nodes by visiting these websites:

* **Libre Mainnet Nodes:** [https://libre.antelope.tools](https://libre.antelope.tools/)
* **Libre Testnet Nodes:** [https://libre-testnet.antelope.tools](https://libre-testnet.antelope.tools/)

All validators must review the [libre-governance-docs](../governance/libre-governance-docs/ "mention") - which outline the minimum requirements and responsibilities.&#x20;

If you want to become a [validator, start here](../earn/become-a-validator.md).
