---
description: Libre is a blockchain for freedom.
---

# Freedom

### **Free to Transact**

Libre has no borders and no transaction fees. All of the wallets are non-custodial and work anywhere in the universe where there is an internet connection. API nodes are provided by the Libre [validators](../validators.md). Libre transactions are nearly instant and irreversible. Libre is built to interact seamlessly with [Lightning, Bitcoin, Ethereum and other chains](broken-reference).

### **Free to Trade**

Libre allows anyone in the world to trade Bitcoin for stablecoins. Libre has no transaction fees and a very low-fee AMM owned and governed by the [Libre DAO](../../governance/dao.md).&#x20;

### **Free to Build**

Anyone can build applications that leverage Bitcoin, Tether, and other tokens on the Libre chain. Apps can be built in [any language that compiles to WASM](https://github.com/appcypher/awesome-wasm-langs) including C, C++, Python, Go, Rust, Java, Javascript and PHP.

### **Free to Earn**

Libre leverages gamification and rewards through [on-chain referrals](../../earn/on-chain-referrals.md) to incentivize user growth and adoption. Libre accounts can [stake their LIBRE to earn LIBRE rewards](../../earn/staking.md).

### Free to Self-Govern

Libre chain uses the LIBRE for [governance](broken-reference) - both [electing validators](../../governance/validator-election.md) as well as [voting in the Libre DAO](../../governance/dao.md). If you are technical and you want to become an integral part of the chain - see the docs on [becoming a validator](../../earn/become-a-validator.md).
