---
description: Libre maximizes usability.
---

# Usability

It's very simple for everyone to use and it's very easy to build on or integrate into any business to receive payments instantly with no transaction fees.

This is quite different from any other blockchain - all of the other blockchain solutions today require everyone to have a token to use the chain. This is a major blocker for new users.&#x20;

Libre was designed to be able to start using in just a few seconds. Just [download a wallet](https://www.libre.org/wallets) and create an account, then try out using one of the [Libre applications](https://www.libre.org/ecosystem).
