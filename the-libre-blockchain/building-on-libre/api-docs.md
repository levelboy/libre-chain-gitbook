---
description: >-
  These docs are generated from balanced Swagger docs  To find a list of
  endpoints, please check Developer Tools or Libre Testnet doc pages.
---

# API Docs

{% hint style="info" %}
Please note that regardless of what the swagger docs below say there is a general rule we recommend for using /v1/ and /v2/ paths:&#x20;

**/v1 use POST**&#x20;

**/v2 use GET**&#x20;

There is a helpful doc outlining [API Methods here](https://docs.google.com/spreadsheets/d/1kGp\_XjhHFZLwzKlGmffuGfNo1NM3xQKaLOM0eqDK7Ak/edit#gid=1588967584) - (built by validator [Cryptobloks](https://cryptobloks.io/))
{% endhint %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v2/health" method="get" expanded="false" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v2/history/get_abi_snapshot" method="get" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v2/history/get_actions" method="get" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v2/history/get_created_accounts" method="get" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v2/history/get_creator" method="get" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v2/history/get_deltas" method="get" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v2/history/get_schedule" method="get" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v2/history/get_transaction" method="get" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v2/state/get_account" method="get" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v2/state/get_key_accounts" method="get" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v2/state/get_key_accounts" method="post" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v2/state/get_links" method="get" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v2/state/get_proposals" method="get" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v2/state/get_tokens" method="get" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v2/state/get_voters" method="get" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v2/stats/get_missed_blocks" method="get" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v1/history/get_actions" method="post" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v1/history/get_controlled_accounts" method="post" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v1/history/get_key_accounts" method="post" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v1/history/get_transaction" method="post" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v1/trace_api/get_block" method="post" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v1/chain/abi_bin_to_json" method="post" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v1/chain/abi_json_to_bin" method="post" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v1/chain/get_abi" method="post" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v1/chain/get_account" method="post" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v1/chain/get_activated_protocol_features" method="post" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v1/chain/get_block" method="post" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v1/chain/get_block_header_state" method="post" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v1/chain/get_code" method="post" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v1/chain/get_currency_balance" method="post" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v1/chain/get_currency_stats" method="post" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v1/chain/get_info" method="post" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v1/chain/get_producers" method="post" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v1/chain/get_raw_abi" method="post" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v1/chain/get_raw_code_and_abi" method="post" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v1/chain/get_scheduled_transaction" method="post" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v1/chain/get_table_by_scope" method="post" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v1/chain/get_table_rows" method="post" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v1/chain/push_transaction" method="post" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v1/chain/push_transactions" method="post" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v1/chain/send_transaction" method="post" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/v1/chain/*" method="post" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}

{% swagger src="https://lb.libre.org/v2/docs/json" path="/stream-client.js" method="get" %}
[https://lb.libre.org/v2/docs/json](https://lb.libre.org/v2/docs/json)
{% endswagger %}
