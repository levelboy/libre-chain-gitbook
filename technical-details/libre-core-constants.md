---
description: These constants are hard-coded in the smart contract.
---

# Libre Core Constants

The following constants will be hard-coded into the LIBRE token contracts and cannot be changed without a hard-fork of the code.

This math is implemented in the [open-source Libre staking contract](https://gitlab.com/libre-chain/staking-contract).

| Constants | Value      | Description              |
| --------- | ---------- | ------------------------ |
| Alpha0    | 10%        | minimum yield            |
| Beta0     | 200%       | maximum yield            |
| AlphaT    | 6%         | terminal min yield       |
| BetaT     | 30%        | terminal max yield       |
| T         | 730        | days to terminal yield   |
| D         | 30         | days in mint rush        |
| B         | 100%       | bonus for day0 mint rush |
| REF       | 10%        | Staking referral bonus   |
| DAO       | 10%        | DAO mint percentage      |
| L         | 2022-07-04 | Launch Date              |



These core constants define in an immutable way the core gamification incentives of the Libre blockchain.\
\
Specifically, the staking returns depend quadratically (square root of time) on the staking period, and also quadratically decay from an initial min and max rates to a terminal rate over a 730 day period. [staking-details.md](staking-details.md "mention")

There is also a bonus staking amount for staking during the first day of the mint rush, which decays linearly over the days of the mint rush. [mint-rush-details.md](mint-rush-details.md "mention")
