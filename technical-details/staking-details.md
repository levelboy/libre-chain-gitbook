# Staking Details

### Libre Staking Math

The staking return is calculated using the following formula:\
$$f(s,t) = min(t) + √ (s) * ( Max(t)-Min(t) )$$\
\
Where `s` is the index of time elapsed since day one of staking, `t` is the staking length.&#x20;

`Min` is a function of time that decreases with `t`, and `Max` is also a function of `t.`&#x20;

The degradation of `Min` and `Max` is also a function of the square root of time `s` and not `t`.\
\
These are core in the inherent game mechanics of the LIBRE token incentives.

This math is implemented in the [open-source Libre staking contract](https://gitlab.com/libre-chain/staking-contract).

### Libre Staking Table

<table><thead><tr><th width="210.33333333333331">Variable</th><th>Type</th><th>Value</th></tr></thead><tbody><tr><td>Stake Date</td><td>DATE</td><td>Date of User-Initiated Stake</td></tr><tr><td>Stake Length</td><td>INT</td><td>User Defined [1-1460 DAYS] **Required**</td></tr><tr><td>Mint Bonus</td><td>%</td><td>Only applies to stakes created in Mint</td></tr><tr><td>Libre Staked</td><td>FLOAT</td><td>User Defined **Required**</td></tr><tr><td>APY</td><td>%</td><td>=((Alpha0+sqrt(StakeLength/365)*(Beta0-Alpha0))+((AlphaT+sqrt(StakeLength/365)*(BetaT-AlphaT))-(Alpha0+sqrt(StakeLength/365)*(Beta0-Alpha0)))*sqrt(MIN(1,(StakeDate-L))/T))*(1+MintBonus)</td></tr><tr><td>Payout</td><td>FLOAT</td><td>=(StakeLength/365) * LibreStaked * APY + Libre Staked</td></tr><tr><td>Payout Date</td><td>DATE</td><td>=StakeDate + StakeLength [1 - 1460 DAYS]</td></tr></tbody></table>
